import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import * as $ from 'jquery';

import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  number;
  porcent;
  const;
  constructor(public navCtrl: NavController, private admob: AdMobFree, private platform: Platform) {
    this.number = "";
    this.porcent = 5.4;
    this.const = 0.30;
  }
  ionViewDidLoad(){
    if(this.platform.is('cordova')){
      const bannerConfig: AdMobFreeBannerConfig = {
       // add your config here
       // for the sake of this example we will just use the test config
       isTesting: false,
       autoShow: true,
       //lo hice yo
       id:'ca-app-pub-2070807448620651/3663206203'
      };
      this.admob.banner.config(bannerConfig);

      this.admob.banner.prepare()
        .then(() => {
          // banner Ad is ready
          // if we set autoShow to false, then we will need to call the show method here
        })
        .catch(e => console.log(e));
    } 
  }
  addNumber(number){
    this.number += String(number);
    let result = (this.number * this.porcent / 100) + this.const;
    $("#amount").text(this.number);
    $("#send").text((Number(this.number)+Number(result)));
    $("#receive").text((Number(this.number)-Number(result)));
  }
  delete(){
    $("#amount, #send, #receive").text('0');
    this.number = "";
  }

}